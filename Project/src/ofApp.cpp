#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	runwayGen.setup(this,"http://localhost:8000");
	runwayGen.start();

	runwayCOCO.setup(this, "http://localhost:8001");
	runwayCOCO.start();
	
	for (int i = 0; i < 512; i++)
	{
		float f = ofRandom(-2, 2);
		v.push_back(f);
	}

	pictureUpdated = false;

	dispImg.allocate(512, 512, ofImageType::OF_IMAGE_COLOR);
	
	AddColorsToList();

}

//--------------------------------------------------------------
void ofApp::update() {
	if (!runwayGen.isBusy() && pictureUpdated == false)
	{
		ofxRunwayData data;
		data.setFloats("z", v);
		runwayGen.send(data);
	}
	runwayGen.get("image", runwayResultGen);
	
	if (runwayResultGen.isAllocated() && pictureUpdated == false && !runwayGen.isBusy())
	{
		runwayResultGen.resize(512, 512);
		int width = runwayResultGen.getWidth();
		int height = runwayResultGen.getHeight();
		for (int y = 0; y < height; y++) 
		{
			for (int x = 0; x < width; x++)
			{
				// Color of the pixel (x,y)
				ofColor myColor = runwayResultGen.getColor(x, y);
				ofColor closestColor = ColorsAreClose(myColor);
				dispImg.setColor(x, y, closestColor);				
			}
		}
		dispImg.update();
		pictureUpdated = true;
		
		
	}

	if(!runwayCOCO.isBusy() && pictureUpdated)
	{
		ofxRunwayData data;
		data.setImage("semantic_map", dispImg, ofxRunwayImageType::OFX_RUNWAY_JPG);
		runwayCOCO.send(data);
	}
	runwayCOCO.get("output", runwayCOCOResult);
	
}



//--------------------------------------------------------------
void ofApp::draw() {
	
	if (runwayCOCOResult.isAllocated() && pictureUpdated)
	{	
		dispImg.draw(0, 0);
		
		runwayCOCOResult.draw(512, 0);
	}
}

void ofApp::keyPressed(int key) {
	if (key == ' ' && pictureUpdated)
	{
		v.clear();
		for (int i = 0; i < 512; i++)
		{
			float f = ofRandom(-2, 2);
			v.push_back(f);
		}
		pictureUpdated = false;		
		//dispImg.clear();
		runwayCOCOResult.clear();
		runwayResultGen.clear();
	}
	if (key == OF_KEY_RETURN)
	{
		ofImage img; 
		img.grabScreen(0,0,1024,512);
		img.save("Screesnhot.jpg");
	}
}

ofColor ofApp::ColorsAreClose(ofColor color)
{
	ofColor closestColor = colors[0];
	for (auto& color1 : colors)
	{
		float rDist = abs(color1.r - color.r);
		float gDist = abs(color1.g - color.g);
		float bDist = abs(color1.b - color.b);

		float rgbDist = sqrt(pow(rDist, 2) + pow(gDist, 2) + pow(bDist, 2));
		float rgbDist1 = 255; 
		if (closestColor != color1)
		{

			float rDist1 = abs(closestColor.r - color1.r);
			float gDist1 = abs(closestColor.g - color1.g);
			float bDist1 = abs(closestColor.b - color1.b);

			rgbDist1 = sqrt(pow(rDist1, 2) + pow(gDist1, 2) + pow(bDist1, 2));
		}

		if (rgbDist < rgbDist1)
		{
			closestColor = color1;
		}
	}
	
	return closestColor;
}

void ofApp::AddColorsToList()
{
	for (int r = 0; r < 255; r++)
	{
		for (int g = 0; g < 255; g ++)
		{
			for (int b = 0; b < 255; b ++)
			{
				if(r%50 == 0 && g%50 == 0 && b%50 == 0)
					colors.push_back(ofColor(r, g, b));
			}
		}
	}


}

// Runway sends information about the current model
//--------------------------------------------------------------
void ofApp::runwayInfoEvent(ofJson& info) {
	ofLogNotice("ofApp::runwayInfoEvent") << info.dump(2);
}
// if anything goes wrong
//--------------------------------------------------------------
void ofApp::runwayErrorEvent(string& message) {
	ofLogNotice("ofApp::runwayErrorEvent") << message;
}
//--------------------------------------------------------------
