#pragma once

#include "ofMain.h"
#include "ofxRunway.h"

class ofApp : public ofBaseApp, public ofxRunwayListener {

	public:
		void setup();
		void update();
		void draw();
		void keyPressed(int key);

		ofColor ColorsAreClose(ofColor color);
		void AddColorsToList();

		// Callback functions that process what Runway sends back
		void runwayInfoEvent(ofJson& info);
		void runwayErrorEvent(string& message);

		bool pictureUpdated;
		
		vector<float> v;
		vector<ofColor> colors;
		ofxRunway runwayGen;
		ofImage runwayResultGen;
		ofImage dispImg;

		ofxRunway runwayCOCO;
		ofImage runwayCOCOResult;


};
